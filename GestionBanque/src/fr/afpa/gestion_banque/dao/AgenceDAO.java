package fr.afpa.gestion_banque.dao;

import fr.afpa.gestion_banque.utils.TableauUtils;
import fr.afpa.gestion_banque.view.AppView;

public class AgenceDAO {

	private static int cptAgence = 1;
	private static String[][] agences = AgenceDAO.generationStub(12);
	

	private static String nextId() {
		return "" + (cptAgence++);
	}

	public static String[] save(String[] newAgence) {
		if (newAgence[AppView.CODE_AGENCE] == null) {
			newAgence[AppView.CODE_AGENCE] = nextId();
		}
		AgenceDAO.agences = TableauUtils.add(AgenceDAO.agences, newAgence);
		return newAgence;
	}

	public static String[][] findAll() {
		return AgenceDAO.agences;
	}

	public static String[] findByCode(String codeAgence) {
		String[][] agences = findAll();
		for (String[] currentAgence : agences) {
			if (currentAgence[AppView.CODE_AGENCE].equalsIgnoreCase(codeAgence))
				return currentAgence;
		}
		return null;
	}

	public static String[][] generationStub(int n) {
		String[][] agences = new String[n][];
		String id;
		
		agences[0] = new String[3];
		agences[0][AppView.CODE_AGENCE] = 0+"" ;
		agences[0][AppView.NOM_AGENCE] = "Agence generique";
		agences[0][AppView.ADRESSE_AGENCE] = "INCONNUE";
		
		for (int i = 2; i <= agences.length; i++) {
			// agences[i] = new String[] {""+i,"agence"+i,"adresse"+i};
			id=nextId();
			agences[i-1] = new String[3];
			agences[i-1][AppView.CODE_AGENCE] = id ;
			agences[i-1][AppView.NOM_AGENCE] = "agence" + i;
			agences[i-1][AppView.ADRESSE_AGENCE] = "adresse" + i;
		}

		return agences;
	}
	
	public static boolean exists(String code) {
		return findByCode(code)!=null;
	}
	
	public static boolean deleteByCode(String codeAgence) {
		String[][] agencesTmp = findAll();
		int indexAgence = -1;
		for (int i = 0; i < agencesTmp.length; i++) {
			if (agencesTmp[i][AppView.CODE_AGENCE].equalsIgnoreCase(codeAgence)) {
				indexAgence = i;
				break;
			}
		}
		if (indexAgence < 0) {
			return false;
		}
		AgenceDAO.agences = TableauUtils.supprimer(agencesTmp, indexAgence);

		return true;
	}

	public static boolean update(String[] newAgence) {
		deleteByCode(newAgence[AppView.CODE_AGENCE]);
		return save(newAgence) != null;
	}

}
