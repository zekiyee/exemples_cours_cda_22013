package fr.afpa.exercices_modelisation;

import java.util.Scanner;

import fr.afpa.exercices_modelisation.entities.Livre;

public class MainLivre {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		int nbrLivre = 3;

		Livre.methodeStatic();
		
		System.out.println(Livre.toto);
		
		if (args.length >= 1) {
			nbrLivre = Integer.parseInt(args[0]);
		}

		Livre[] livres = new Livre[nbrLivre];

		for (int i = 0; i < livres.length; i++) {
			livres[i] = lireInfosLivre();
		}
		/*
		 * Livre livre; for (int i = 0; i < livres.length; i++) { livre = livres[0];
		 * livre = lireInfosLivre(cpt++); } <==> for (Livre livre : livres) { livre =
		 * lireInfosLivre(cpt++); }
		 */

		for (Livre livre : livres) {
			System.out.println(livre);
		}

		System.out.println("Le nombre de livres est " + livres.length);

	}

	private static Livre lireInfosLivre() {

		Livre livre = new Livre();

		System.out.print("Donner le titre du livre n� " + livre.getId() + " : ");
		livre.setTitre(scanner.nextLine());
		System.out.print("Donner l'auteur du livre n� " + livre.getId() + " : ");
		livre.setAuteur(scanner.nextLine());
		System.out.print("Donner le prix du livre n�  " + livre.getId() + " : ");
		livre.setPrix(Double.parseDouble(scanner.nextLine()));

		return livre;
	}

	/*
	 * private static Livre lireInfosLivre2(int id) {
	 * 
	 * System.out.print("Donner le titre du livre n� " + id + " : "); String titre =
	 * scanner.nextLine(); System.out.print("Donner l'auteur du livre n� " + id +
	 * " : "); String auteur = scanner.nextLine();
	 * System.out.print("Donner le prix du livre n�  " + id + " : "); double prix =
	 * Double.parseDouble(scanner.nextLine());
	 * 
	 * return new Livre(id,titre,auteur,prix); }
	 */

	public void name() {
		System.out.println(Livre.cpt);
		// System.out.println(????????.auteur);

		Livre livre = new Livre();

		livre.methodeNonStatic();
		// livre.methodeStatic();
		System.out.println(livre.auteur);

		// Livre.methodeNonStatic();
		System.out.println(Livre.cpt);
		Livre.methodeStatic();

		System.out.println(Livre.cpt);
		System.out.println(livre.auteur);

	}
}
