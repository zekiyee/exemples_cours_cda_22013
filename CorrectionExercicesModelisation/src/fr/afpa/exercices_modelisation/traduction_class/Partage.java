package fr.afpa.exercices_modelisation.traduction_class;

public class Partage {

	private Client client;
	private CapitaleFoncier capitaleFoncier;
	private int part;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public CapitaleFoncier getCapitaleFoncier() {
		return capitaleFoncier;
	}

	public void setCapitaleFoncier(CapitaleFoncier capitaleFoncier) {
		this.capitaleFoncier = capitaleFoncier;
	}

	public int getPart() {
		return part;
	}

	public void setPart(int part) {
		this.part = part;
	}

}
