package fr.afpa.gestion_banque.dao;

import java.util.Random;

import fr.afpa.gestion_banque.utils.TableauUtils;
import fr.afpa.gestion_banque.view.AppView;

public class ClientDAO {

	private static int cptClient = 1;
	private static String[][] clients = ClientDAO.generationStub(12);

	private static String nextId() {
		String chaineFormatte = String.format("%06d", cptClient++);
		return "AB" + chaineFormatte;
	}

	public static String[] save(String[] newClient) {
		if (newClient[AppView.ID_CLIENT] == null) {
			newClient[AppView.ID_CLIENT] = nextId();
		}
		ClientDAO.clients = TableauUtils.add(ClientDAO.clients, newClient);
		return newClient;
	}

	public static String[][] findAll() {
		return ClientDAO.clients;
	}

	public static String[] findById(String id) {
		String[][] clients = findAll();
		for (String[] currentClient : clients) {
			if (currentClient[AppView.ID_CLIENT].equalsIgnoreCase(id))
				return currentClient;
		}
		return null;
	}

	public static String[][] findByNom(String nom) {
		String[][] clients = findAll();
		String[][] clientsResultats = new String[0][];
		for (String[] currentClient : clients) {
			if (currentClient[AppView.NOM_CLIENT].equalsIgnoreCase(nom))
				clientsResultats = TableauUtils.add(clientsResultats, currentClient);
		}
		return clientsResultats;
	}

	public static String[] findByNumeroCompte(String idCompte) {
		String[] compteBancaire = CompteBancaireDAO.findById(idCompte);
		if (compteBancaire == null)
			return null;

		return findById(compteBancaire[AppView.CLIENT_ID_CB]);
	}

	public static boolean exists(String id) {
		return findById(id) != null;
	}

	public static String[][] generationStub(int n) {
		String[][] clients = null;
		for (int i = 1; i <= n; i++) {
			clients = TableauUtils.add(clients,unClient(i));
		}
		return clients;
	}

	private static String[] unClient(int i) {
		Random ran = new Random();
		String[] currentClient = new String[0];
		String id = nextId();
		currentClient = new String[5];
		currentClient[AppView.ID_CLIENT] = id;
		String nomClient = "nom_client" + i;
		if(ran.nextBoolean())
			nomClient = "toto";
		currentClient[AppView.NOM_CLIENT] = nomClient;
		currentClient[AppView.PRENOM_CLIENT] = "prenom_client" + i;
		currentClient[AppView.DATE_NAISSANCE_CLIENT] = "client" + i;
		currentClient[AppView.MAIL_CLIENT] = "nom_client" + i + "@gmail.com";
		return currentClient;
	}

	public static boolean deleteById(String id) {
		String[][] clientsTmp = findAll();
		int indexClient = -1;
		for (int i = 0; i < clientsTmp.length; i++) {
			if (clientsTmp[i][AppView.ID_CLIENT].equalsIgnoreCase(id)) {
				indexClient = i;
				break;
			}
		}
		if (indexClient < 0) {
			return false;
		}
		ClientDAO.clients = TableauUtils.supprimer(clientsTmp, indexClient);

		return true;
	}

	public static boolean update(String[] newClient) {
		deleteById(newClient[AppView.ID_CLIENT]);
		return save(newClient) != null;
	}

}
