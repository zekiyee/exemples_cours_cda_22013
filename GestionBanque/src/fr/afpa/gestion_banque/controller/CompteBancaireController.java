package fr.afpa.gestion_banque.controller;

import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.view.AppView;
import fr.afpa.gestion_banque.view.CompteBancaireView;
import fr.afpa.gestion_banque.view.validator.CompteBancairesValidator;

public class CompteBancaireController {
	public static void creation() {
		String[] newCompteBancaire = CompteBancaireView.affichageCreation();

		boolean isOk = CompteBancairesValidator.verificationRGs(newCompteBancaire);

		if (!isOk) {
			return;
		}
		newCompteBancaire = CompteBancaireDAO.save(newCompteBancaire);

		AppView.affichageMessage("Le compteBancaire " + newCompteBancaire[AppView.ID_CB] + " est cr��");
	}

	public static void affichageAll() {
		String[][] compteBancaires = CompteBancaireDAO.findAll();
		CompteBancaireView.affichageTous(compteBancaires);
	}

	public static void suppression() {
		String idCompteBancaire = CompteBancaireView.affichageSuppression();

		if (!CompteBancaireDAO.exists(idCompteBancaire)) {
			AppView.affichageMessage("Le compteBancaire " + idCompteBancaire + " n'existe pas");
			return;
		}
		CompteBancaireDAO.deleteById(idCompteBancaire);
		AppView.affichageMessage("Le compteBancaire " + idCompteBancaire + " est supprime");
	}

	public static void modification() {
		String idCompteBancaire = CompteBancaireView.recuperationIdCompteBancaire("Modification CompteBancaire : ");
		String[] oldCompteBancaire = CompteBancaireDAO.findById(idCompteBancaire);
		if (oldCompteBancaire == null) {
			AppView.affichageMessage("Le compteBancaire " + idCompteBancaire + " n'existe pas");
			return;
		}
		String[] newCompteBancaire = CompteBancaireView.affichageModification(oldCompteBancaire);
		CompteBancaireDAO.update(newCompteBancaire);
		AppView.affichageMessage("Le compteBancaire " + newCompteBancaire[AppView.ID_CLIENT] + " est modifie");

	}

	public static void chercherUnCompteBancaire() {
		String idCompteBancaire = CompteBancaireView
				.recuperationIdCompteBancaire("Rechercher un compteBancaire par numero: ");
		String[] currentCompteBancaire = CompteBancaireDAO.findById(idCompteBancaire);
		if (currentCompteBancaire == null) {
			AppView.affichageMessage("Le compteBancaire " + idCompteBancaire + " n'existe pas");
			return;
		}
		String[][] compteBancaires = new String[][] { currentCompteBancaire };
		CompteBancaireView.affichageTous(compteBancaires);

	}
	
	public static void chercherTousCompteBancaireClient() {
		String idClient = CompteBancaireView
				.recuperationIdCompteBancaire("Afficher la liste des comptes d�un client : ");
		String[][] compteBancaires = CompteBancaireDAO.findAllByIdClient(idClient);
		if (compteBancaires == null || compteBancaires.length==0) {
			AppView.affichageMessage("Le client " + idClient + " ne possede pas de comptes bancaires");
			return;
		}
		CompteBancaireView.affichageTous(compteBancaires);

	}
}
