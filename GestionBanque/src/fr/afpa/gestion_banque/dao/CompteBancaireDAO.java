package fr.afpa.gestion_banque.dao;

import java.util.Random;

import fr.afpa.gestion_banque.utils.TableauUtils;
import fr.afpa.gestion_banque.view.AppView;

public class CompteBancaireDAO {

	private static int cptCompteBancaire = 1;
	private static String[][] compteBancaires = CompteBancaireDAO.generationStub(7);

	private static String nextId() {
		return "" + (cptCompteBancaire++);
	}

	public static String[] save(String[] newCompteBancaire) {
		if (newCompteBancaire[AppView.ID_CB] == null) {
			newCompteBancaire[AppView.ID_CB] = nextId();
		}
		CompteBancaireDAO.compteBancaires = TableauUtils.add(CompteBancaireDAO.compteBancaires, newCompteBancaire);
		return newCompteBancaire;
	}

	public static String[][] findAll() {
		return CompteBancaireDAO.compteBancaires;
	}

	public static String[] findById(String id) {
		String[][] compteBancaires = findAll();
		for (String[] currentCompteBancaire : compteBancaires) {
			if (currentCompteBancaire[AppView.ID_CB].equalsIgnoreCase(id))
				return currentCompteBancaire;
		}
		return null;
	}

	public static String[][] findAllByCodeAgence(String codeAgence) {
		String[][] compteBancaires = findAll();
		String[][] resultatsCompteBancaires = new String[0][];
		for (String[] currentCompteBancaire : compteBancaires) {
			if (currentCompteBancaire[AppView.AGENCE_CODE_CB].equalsIgnoreCase(codeAgence)) {
				resultatsCompteBancaires = TableauUtils.add(resultatsCompteBancaires, currentCompteBancaire);
			}
		}
		return resultatsCompteBancaires;
	}
	
	public static String[][] findAllByIdClient(String idClient) {
		String[][] compteBancaires = findAll();
		String[][] resultatsCompteBancaires = new String[0][];
		for (String[] currentCompteBancaire : compteBancaires) {
			if (currentCompteBancaire[AppView.CLIENT_ID_CB].equalsIgnoreCase(idClient)) {
				resultatsCompteBancaires = TableauUtils.add(resultatsCompteBancaires, currentCompteBancaire);
			}
		}
		return resultatsCompteBancaires;
	}

	public static int countClientCompteBancaire(String idClient) {
		String[][] compteBancaires = findAllByIdClient(idClient);
		return compteBancaires.length;
	}

	public static boolean aPlusQue3CompteComptes(String idClient) {
		return countClientCompteBancaire(idClient) >= 3;
	}

	public static boolean aUnCompteBancaireDuType(String idClient, String type) {
		String[][] compteBancaires = findAllByIdClient(idClient);
		for (String[] currentCompte : compteBancaires) {
			if (currentCompte[AppView.TYPE_CB].equalsIgnoreCase(type))
				return true;
		}

		return false;
	}

	public static String[][] generationStub(int nombreClient) {
		String[][] compteBancaires = null;
		String idClient;
		for (int i = 1; i <= nombreClient; i++) {
			idClient = "AB" + String.format("%06d", i);
			compteBancaires = TableauUtils.add(compteBancaires, plusieursCb(idClient));
		}
		return compteBancaires;
	}

	private static String[][] plusieursCb(String idClient) {
		String[][] comptesbancaires = null;
		Random ran = new Random();
		int nombreComptesBancaires = ran.nextInt(3 + 1 - 1) + 1;

		if (nombreComptesBancaires >= 1) {
			comptesbancaires = TableauUtils.add(comptesbancaires, unCB(idClient, AppView.COMPTE_COURANT));
		}
		if (nombreComptesBancaires >= 2) {
			comptesbancaires = TableauUtils.add(comptesbancaires, unCB(idClient, AppView.LIVRET_A));
		}
		if (nombreComptesBancaires >= 3) {
			comptesbancaires = TableauUtils.add(comptesbancaires, unCB(idClient, AppView.PLAN_EPARGNE_LOGEMENT));
		}

		return comptesbancaires;
	}

	private static String[] unCB(String idClient, String typeCompte) {
		Random ran = new Random();
		String id = nextId();
		String codeAgence = (ran.nextInt(10+1-1)+1)+"";
		String[] newCompteBancaire = new String[6];
		newCompteBancaire[AppView.ID_CB] = id;
		newCompteBancaire[AppView.SOLDE_CB] = ran.nextInt(10000001) + "";
		newCompteBancaire[AppView.DECOUVERT_CB] = ran.nextBoolean() + "";
		newCompteBancaire[AppView.TYPE_CB] = typeCompte;
		newCompteBancaire[AppView.CLIENT_ID_CB] = idClient;
		newCompteBancaire[AppView.AGENCE_CODE_CB] =  codeAgence;
		return newCompteBancaire;
	}

	public static boolean exists(String id) {
		return findById(id)!=null;
	}

	
	public static boolean deleteById(String id) {
		String[][] compteBancairesTmp = findAll();
		int indexCompteBancaire = -1;
		for (int i = 0; i < compteBancairesTmp.length; i++) {
			if (compteBancairesTmp[i][AppView.ID_CB].equalsIgnoreCase(id)) {
				indexCompteBancaire = i;
				break;
			}
		}
		if (indexCompteBancaire < 0) {
			return false;
		}
		CompteBancaireDAO.compteBancaires = TableauUtils.supprimer(compteBancairesTmp, indexCompteBancaire);

		return true;
	}

	public static boolean update(String[] newCompteBancaire) {
		deleteById(newCompteBancaire[AppView.ID_CB]);
		return save(newCompteBancaire) != null;
	}

}
