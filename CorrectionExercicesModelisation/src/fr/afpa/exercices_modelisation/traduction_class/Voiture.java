package fr.afpa.exercices_modelisation.traduction_class;

public class Voiture {

	private int id;
	private String marque;
	private String modele;

	private Client[] clients;

	public Voiture() {
		super();
	}

	public Voiture(int id, String marque, String modele) {
		super();
		this.id = id;
		this.marque = marque;
		this.modele = modele;
	}

	public Voiture(int id, String marque, String modele, Client[] clients) {
		super();
		this.id = id;
		this.marque = marque;
		this.modele = modele;
		this.clients = clients;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public Client[] getClients() {
		return clients;
	}

	public void setClients(Client[] clients) {
		this.clients = clients;
	}

}
