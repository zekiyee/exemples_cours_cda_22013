package fr.afpa.gestion_banque.view;
public class AgenceView {

	public static String[] affichageCreation() {
		String[] newAgence = new String[3];
		System.out.println("Creation Agence : ");
		System.out.print("Nom : ");
		newAgence[AppView.NOM_AGENCE] = AppView.scanner.nextLine().trim();
		System.out.print("Adresse : ");
		newAgence[AppView.ADRESSE_AGENCE] = AppView.scanner.nextLine().trim();

		return newAgence;
	}

	public static void affichageToutes(String[][] agences) {
		System.out.println("Liste agences : ");
		System.out.printf("%-15s\t%-15s\t%s\t", "CODE_AGENCE", "NOM_AGENCE", "ADRESSE_AGENCE");
		System.out.print(System.lineSeparator());
		for (int i = 0; i < agences.length; i++) {
			String[] currentAgence = agences[i];
			System.out.printf(" %-14s\t %-14s\t %s\t", currentAgence[AppView.CODE_AGENCE],
					currentAgence[AppView.NOM_AGENCE], currentAgence[AppView.ADRESSE_AGENCE]);
			System.out.print(System.lineSeparator());
		}
		/*
		 * for (String[] currentAgence : agences) {
		 * System.out.printf("%10s\t%10s\t%10s\t",currentAgence[0],currentAgence[1],
		 * currentAgence[2]); System.out.print(System.lineSeparator()); }
		 */
	}

	public static String affichageSuppression() {
		System.out.println("Suppression Agence : ");
		System.out.print("		Code agence : ");
		String codeAgence = AppView.scanner.nextLine().trim();
		return codeAgence;
	}

	public static String[] affichageModification(String[] oldAgence) {

		System.out.print("Nom (" + oldAgence[AppView.NOM_AGENCE] + "): ");
		String newNomAgence = AppView.scanner.nextLine().trim();
		if (!newNomAgence.isBlank() && !newNomAgence.isEmpty())
			oldAgence[AppView.NOM_AGENCE] = newNomAgence;
		System.out.print("Adresse (" + oldAgence[AppView.ADRESSE_AGENCE] + "): ");
		String newAdresseAgence = AppView.scanner.nextLine().trim();
		if (!newAdresseAgence.isBlank() && !newAdresseAgence.isEmpty())
			oldAgence[AppView.ADRESSE_AGENCE] = newAdresseAgence;

		return oldAgence;
	}

	public static String recuperationCodeAgence() {
		String codeAgence;
		System.out.println("Modification Agence : ");
		System.out.print("		Code agence : ");
		codeAgence = AppView.scanner.nextLine().trim();
		return codeAgence;
	}
}
