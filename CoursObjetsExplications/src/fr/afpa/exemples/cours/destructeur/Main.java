package fr.afpa.exemples.cours.destructeur;

public class Main {
	public static void main(String[] args) throws InterruptedException {
		MaClasse maClasse = new MaClasse();
		
		System.out.println( maClasse.connexion);
		
		maClasse.faireUneRequete("test");
		maClasse.faireUneRequete("test1");
		maClasse.faireUneRequete("test2");
		
		maClasse=null;
		System.out.println("Je vais dormir 30s");
		System.gc();
		Thread.sleep(30000);
		System.out.println("Fin du programme");
	}

}
