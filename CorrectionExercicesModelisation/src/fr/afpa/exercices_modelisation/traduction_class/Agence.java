package fr.afpa.exercices_modelisation.traduction_class;

public class Agence {

	private int code;
	private String nom;
	// bi
	// private CompteBancaire[] compteBancaires;
	private Adresse adresse;

	public Agence() {

	}

	public Agence(int code, String nom) {
		super();
		this.code = code;
		this.nom = nom;
	}

	public Agence(int code, String nom, Adresse adresse) {
		super();
		this.code = code;
		this.nom = nom;
		this.adresse = adresse;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

}
