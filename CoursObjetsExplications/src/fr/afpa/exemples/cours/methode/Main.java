package fr.afpa.exemples.cours.methode;

import fr.afpa.exemples.cours.constructeurs.Voiture;

public class Main {
	
	
	public static void main(String[] args) {
		int a= 33;
		Voiture v = new  Voiture("BMW","118");
		System.out.println(a);
		System.out.println(v);
		System.out.println(v.marque);
		methode1(a, v);
		System.out.println(a);
		System.out.println(v);
		System.out.println(v.marque);
		
		Voiture v2 = v.clone();
		Voiture v3 = new Voiture(v);
	}
	
	public static void methode1(int a, Voiture v){
		//v = new Voiture();
		System.out.println(a);
		a=23;
		System.out.println(a);
		System.out.println(v);
		System.out.println(v.marque);
		v.marque="mercedes";
		System.out.println(v);
		System.out.println(v.marque);
	}

}
