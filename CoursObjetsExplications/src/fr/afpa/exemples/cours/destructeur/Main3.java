package fr.afpa.exemples.cours.destructeur;

public class Main3 {
	public static void main(String[] args) throws InterruptedException {
		MaClasse maClasse;
		MaClasse[] mesClasses = new MaClasse[0];
		for (;;) {
			maClasse = new MaClasse();
			// System.out.println(maClasse.connexion);
			// maClasse.faireUneRequete("test");
			// maClasse.faireUneRequete("test1");
			// maClasse.faireUneRequete("test2");
			mesClasses = TableauUtils.add(mesClasses, maClasse);
			System.out.println(mesClasses.length);
		}

	}

}
