package fr.afpa.gestion_banque.controller;

import fr.afpa.gestion_banque.dao.AgenceDAO;
import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.view.AgenceView;
import fr.afpa.gestion_banque.view.AppView;

public class AgenceController {
	public static void creation() {
		String[] newAgence = AgenceView.affichageCreation();
		newAgence = AgenceDAO.save(newAgence);
		AppView.affichageMessage("L'agence " + newAgence[AppView.CODE_AGENCE] + " est cr��e");
	}

	public static void affichageAll() {
		String[][] agences = AgenceDAO.findAll();
		AgenceView.affichageToutes(agences);
	}

	public static void suppression() {
		String codeAgence = AgenceView.affichageSuppression();
		if ("0".equals(codeAgence)) {
			AppView.affichageMessage("impossible de supprimer l'agence par defaut");
			return;
		}
		if (!AgenceDAO.exists(codeAgence)) {
			AppView.affichageMessage("L'agence " + codeAgence + " n'existe pas");
			return;
		}
		
		String [][] comptesBancaires = CompteBancaireDAO.findAllByCodeAgence(codeAgence);
		
		for (String[] currentCB : comptesBancaires) {
			currentCB[AppView.AGENCE_CODE_CB]="0";
			CompteBancaireDAO.update(currentCB);
		}

		AgenceDAO.deleteByCode(codeAgence);

		AppView.affichageMessage("L'agence " + codeAgence + " est supprimee");
	}

	public static void modification() {
		String codeAgence = AgenceView.recuperationCodeAgence();
		String[] oldAgence = AgenceDAO.findByCode(codeAgence);
		if (oldAgence == null) {
			AppView.affichageMessage("L'agence " + codeAgence + " n'existe pas");
			return;
		}
		String[] newAgence = AgenceView.affichageModification(oldAgence);
		AgenceDAO.update(newAgence);
		AppView.affichageMessage("L'agence " + newAgence[AppView.CODE_AGENCE] + " est modifi�e");

	}
}
