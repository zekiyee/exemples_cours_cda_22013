package fr.afpa.exercices_modelisation.entities;

public class LivreSansStatic {
	private int id;
	private String titre;
	public String auteur;
	private double prix;

	public LivreSansStatic() {
	}

	public LivreSansStatic(int id) {
		this.id = id;
	}

	public LivreSansStatic(int id, String titre, String auteur, double prix) {
		this.id = id;
		this.titre = titre;
		this.auteur = auteur;
		this.prix = prix;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Le prix du livre " + this.titre + " de l'auteur " + this.auteur + " est :" + this.prix + " DH";
	}

}
