package fr.afpa.exemples.cours.destructeur;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

public class LargeMaClassFinalizer extends PhantomReference<MaClasse> {

    public LargeMaClassFinalizer(MaClasse referent, ReferenceQueue<? super MaClasse> q) {
        super(referent, q);
    }

    public void finalizeResources() {
        System.out.println("clearing ...");
    }
}