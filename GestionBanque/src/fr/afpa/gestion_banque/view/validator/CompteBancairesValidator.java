package fr.afpa.gestion_banque.view.validator;

import fr.afpa.gestion_banque.dao.AgenceDAO;
import fr.afpa.gestion_banque.dao.ClientDAO;
import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.view.AppView;

public class CompteBancairesValidator {
	public static boolean verificationRGs(String[] newCompteBancaire) {
		String[] client = ClientDAO.findById(newCompteBancaire[AppView.CLIENT_ID_CB]);
		if (client == null) {
			System.out.println("Le client " + newCompteBancaire[AppView.CLIENT_ID_CB] + " n'existe pas");
			return false;
		}

		String[] agence = AgenceDAO.findByCode(newCompteBancaire[AppView.AGENCE_CODE_CB]);
		if (agence == null) {
			System.out.println("L'agence " + newCompteBancaire[AppView.AGENCE_CODE_CB] + " n'existe pas");
			return false;
		}

		if (!newCompteBancaire[AppView.TYPE_CB].equalsIgnoreCase(AppView.COMPTE_COURANT)
				&& !newCompteBancaire[AppView.TYPE_CB].equalsIgnoreCase(AppView.LIVRET_A)
				&& !newCompteBancaire[AppView.TYPE_CB].equalsIgnoreCase(AppView.PLAN_EPARGNE_LOGEMENT)) {
			System.out.println("Le type " + newCompteBancaire[AppView.TYPE_CB] + " n'existe pas");
			return false;
		}

		if (CompteBancaireDAO.aPlusQue3CompteComptes(newCompteBancaire[AppView.CLIENT_ID_CB])) {
			System.out.println("Le client " + newCompteBancaire[AppView.CLIENT_ID_CB] + " a deja 3 comptes");
			return false;
		}

		if (CompteBancaireDAO.aUnCompteBancaireDuType(newCompteBancaire[AppView.CLIENT_ID_CB],
				newCompteBancaire[AppView.TYPE_CB])) {
			System.out.println("Le client " + newCompteBancaire[AppView.CLIENT_ID_CB] + " a deja un comptedu type <"
					+ newCompteBancaire[AppView.TYPE_CB] + ">");
			return false;
		}

		return true;
	}
}
