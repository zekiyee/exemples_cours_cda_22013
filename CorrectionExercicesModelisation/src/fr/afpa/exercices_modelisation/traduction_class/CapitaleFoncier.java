package fr.afpa.exercices_modelisation.traduction_class;

public class CapitaleFoncier {

	private int id;
	private double montant;
	//bi
	private Partage[] partages;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public Partage[] getPartages() {
		return partages;
	}
	public void setPartages(Partage[] partages) {
		this.partages = partages;
	}
	
	
	
}
