package fr.afpa.gestion_banque.view;

import java.util.Scanner;

public class AppView {
	public static Scanner scanner = new Scanner(System.in);
	public static final int CODE_AGENCE = 0;
	public static final int NOM_AGENCE = 1;
	public static final int ADRESSE_AGENCE = 2;

	public static final int ID_CLIENT = 0;
	public static final int NOM_CLIENT = 1;
	public static final int PRENOM_CLIENT = 2;
	public static final int DATE_NAISSANCE_CLIENT = 3;
	public static final int MAIL_CLIENT = 4;

	public static final String COMPTE_COURANT = "CC";
	public static final String LIVRET_A = "LA";
	public static final String PLAN_EPARGNE_LOGEMENT = "PEL";

	public static final int ID_CB = 0;
	public static final int SOLDE_CB = 1;
	public static final int DECOUVERT_CB = 2;
	public static final int TYPE_CB = 3;
	public static final int CLIENT_ID_CB = 4;
	public static final int AGENCE_CODE_CB = 5;

	public static String getTypeCompteNom(String codeType) {
			
		if ("CC".equalsIgnoreCase(codeType))
			return "Compte courant";
		if ("LA".equalsIgnoreCase(codeType))
			return "Livret A";
		if ("PEL".equalsIgnoreCase(codeType))
			return "Plan epargne logement";

		return null;
	}

	public static String affichageMenu() {
		System.out.println("1-  Cr�er une agence");
		System.out.println("2-  Cr�er un client");
		System.out.println("3-  Cr�er un compte bancaire");
		System.out.println("4-  Modifier une agence");
		System.out.println("5-  Modifier un client");
		System.out.println("6-  Modifier un compte bancaire");
		System.out.println("7-  Supprimer une agence");
		System.out.println("8-  Supprimer un client");
		System.out.println("9-  Supprimer un compte bancaire");
		System.out.println("10- Afficher la liste de tous les agences");
		System.out.println("11- Afficher la liste de tous les clients");
		System.out.println("12- Afficher la liste de tous les comptes bancaires");
		System.out.println("13- Recherche de compte (num�ro de compte)");
		System.out.println("14- Recherche de client (Nom, Num�ro de compte, identifiant de client)");
		System.out.println("15- Afficher la liste des comptes d�un client (identifiant client)");
		System.out.println("16- Imprimer les infos client (identifiant client)");
		System.out.println("17- Quitter le programme");
		System.out.print("\n		Choix : ");

		return AppView.scanner.nextLine().trim();
	}

	public static void affichageMessage(String message) {
		System.out.println(message);
	}
}
