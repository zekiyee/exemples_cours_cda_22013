package fr.afpa.exemples.cours.tableaux;

import java.util.Arrays;

import fr.afpa.exemples.cours.constructeurs.Voiture;

public class Main {

	public static void main(String[] args) {
		int[] t1 = new int[12];
		boolean[] t2 = new boolean[12];
		char[] t3 = new char[12];
		Integer[] t4 = new Integer[12];
		String[] t5 = new String[12];
		Voiture[] t6 = new Voiture[12];
		
		System.out.println(Arrays.toString(t1));
		System.out.println(Arrays.toString(t2));
		System.out.println(Arrays.toString(t3));
		System.out.println(Arrays.toString(t4));
		System.out.println(Arrays.toString(t5));
		System.out.println(Arrays.toString(t6));
		
	}

}
