package fr.afpa.exercices_modelisation.entities;

import java.util.Objects;

public class Livre {
	public static int cpt = 0;
	private int id = 33;
	private String titre = "";
	public String auteur= "";
	private double prix = 0;
	public static int toto = 33;
	
	static {
		//contructeur static 
		cpt = 1;
		toto=45;
	}

	public Livre() {
		this.id= Livre.cpt++;
	}

	public Livre(int id) {
		this.id = id;
	}

	public Livre(int id, String titre, String auteur, double prix) {
		this.id = Livre.cpt++;
		this.titre = titre;
		this.auteur = auteur;
		this.prix = prix;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Le prix du livre " + this.titre + " de l'auteur " + this.auteur + " est :" + this.prix + " DH";
	}

	@Override
	public int hashCode() {
		return Objects.hash(auteur, cpt, id, prix, titre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Livre other = (Livre) obj;
		return Objects.equals(auteur, other.auteur) && cpt == other.cpt && id == other.id
				&& Double.doubleToLongBits(prix) == Double.doubleToLongBits(other.prix)
				&& Objects.equals(titre, other.titre);
	}
	
	public void methodeNonStatic() {
		System.out.println(this.auteur);
	}
	public static void methodeStatic() {
		System.out.println(cpt);
	}
}
