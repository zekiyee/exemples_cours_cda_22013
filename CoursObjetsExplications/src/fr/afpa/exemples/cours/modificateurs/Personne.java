package fr.afpa.exemples.cours.modificateurs;
public class Personne{
	private int id;
	String nom;
	public String prenom;
	
	private void mangerPrivate(String nom){
		Personne2 p2 = new Personne2();
		this.nom = nom ;
		this.id = 22;
		this.prenom="tata";
	}
	protected void mangerProtected(String nom){
		
		this.nom = nom ;
		this.id = 22;
		this.prenom="tata";
	}
	public void mangerPublic(String nom){
		this.nom = nom ;
		this.id = 22;
		this.prenom="tata";
	}
	public void name() {
		this.mangerPrivate(nom);
		this.mangerProtected(nom);
		this.mangerPublic(nom);
	}
}

class Personne2{
	private int id;
	String nom;
	public String prenom;
	
	private void mangerPrivate(String nom){
		this.nom = nom ;
		this.id = 22;
		this.prenom="tata";
	}
	protected void mangerProtected(String nom){
		
		this.nom = nom ;
		this.id = 22;
		this.prenom="tata";
	}
	public void mangerPublic(String nom){
		this.nom = nom ;
		this.id = 22;
		this.prenom="tata";
	}
	public void name() {
		this.mangerPrivate(nom);
		this.mangerProtected(nom);
		this.mangerPublic(nom);
	}
}