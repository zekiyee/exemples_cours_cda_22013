package fr.afpa.exemples.cours.constructeurs;

public class Main {
	public static void main(String[] args) {
		Voiture v0= new Voiture();
		Voiture v1 = new Voiture("BMW","118");
		
		

		System.out.println("Est demarree : "+v1.estDemarree);
		v1.accelerer(30);
		System.out.println("la vitesse actuelle est : "+v1.vitesse);
		v1.start();
		System.out.println("Est demarree : "+v1.estDemarree);
		v1.accelerer(30);
		System.out.println("la vitesse actuelle est : "+v1.vitesse);
	}
}
