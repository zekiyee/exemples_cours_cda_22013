package fr.afpa.gestion_banque.view;

import fr.afpa.gestion_banque.dao.ClientDAO;

public class ClientView {

	public static String[] affichageCreation() {
		String[] newClient = new String[5];
		System.out.println("Creation Client : ");
		System.out.print("Nom : ");
		newClient[AppView.NOM_CLIENT] = AppView.scanner.nextLine().trim();
		System.out.print("Prenom : ");
		newClient[AppView.PRENOM_CLIENT] = AppView.scanner.nextLine().trim();
		System.out.print("Date naissance (dd-MM-aaaa): ");
		newClient[AppView.DATE_NAISSANCE_CLIENT] = AppView.scanner.nextLine().trim();
		System.out.print("Mail : ");
		newClient[AppView.MAIL_CLIENT] = AppView.scanner.nextLine().trim();

		return newClient;
	}

	public static void affichageTous(String[][] clients) {
		System.out.println("Liste clients : ");
		System.out.printf("%-8s\t%-15s\t%-15s\t%-18s\t%s", "ID", "NOM", "PRENOM", "DATE_NAISSANCE", "MAIL");
		System.out.print(System.lineSeparator());
		for (int i = 0; i < clients.length; i++) {
			String[] currentClient = clients[i];
			System.out.printf("%-8s\t%-15s\t%-15s\t%-18s\t%s", currentClient[AppView.ID_CLIENT],
					currentClient[AppView.NOM_CLIENT], currentClient[AppView.PRENOM_CLIENT],
					currentClient[AppView.DATE_NAISSANCE_CLIENT], currentClient[AppView.MAIL_CLIENT]);
			System.out.print(System.lineSeparator());
		}
		/*
		 * for (String[] currentClient : clients) {
		 * System.out.printf("%10s\t%10s\t%10s\t",currentClient[0],currentClient[1],
		 * currentClient[2]); System.out.print(System.lineSeparator()); }
		 */
	}

	public static void main(String[] args) {
		ClientView.affichageTous(ClientDAO.findAll());
	}

	public static String affichageSuppression() {
		System.out.println("Suppression Client : ");
		System.out.print("		Id client : ");
		String idClient = AppView.scanner.nextLine().trim();
		return idClient;
	}

	public static String[] affichageModification(String[] oldClient) {

		System.out.print("Nom (" + oldClient[AppView.NOM_CLIENT] + "): ");
		String newNomClient = AppView.scanner.nextLine().trim();
		if (!newNomClient.isBlank() && !newNomClient.isEmpty())
			oldClient[AppView.NOM_CLIENT] = newNomClient;

		System.out.print("Prenom (" + oldClient[AppView.PRENOM_CLIENT] + "): ");
		String newPrenomClient = AppView.scanner.nextLine().trim();
		if (!newPrenomClient.isBlank() && !newPrenomClient.isEmpty())
			oldClient[AppView.PRENOM_CLIENT] = newPrenomClient;

		System.out.print("Date de naissance (" + oldClient[AppView.DATE_NAISSANCE_CLIENT] + "): ");
		String newDateClient = AppView.scanner.nextLine().trim();
		if (!newDateClient.isBlank() && !newDateClient.isEmpty())
			oldClient[AppView.DATE_NAISSANCE_CLIENT] = newDateClient;

		System.out.print("Mail (" + oldClient[AppView.MAIL_CLIENT] + "): ");
		String newMailClient = AppView.scanner.nextLine().trim();
		if (!newMailClient.isBlank() && !newMailClient.isEmpty())
			oldClient[AppView.MAIL_CLIENT] = newMailClient;
		return oldClient;
	}

	public static String recuperationIdClient() {
		String idClient;
		System.out.println("Modification Client : ");
		System.out.print("		ID client : ");
		idClient = AppView.scanner.nextLine().trim();
		return idClient;
	}
	
	public static String recuperationCritereRecherche() {
		String idClient;
		System.out.println("Recherche de client (Nom, Num�ro de compte, identifiant de client) : ");
		System.out.print("		Choix : ");
		idClient = AppView.scanner.nextLine().trim();
		return idClient;
	}

	public static String recuperationInfosImpression() {
		
		String idClient;
		System.out.println("Imprimer les infos client (identifiant client) : ");
		System.out.print("		ID client : ");
		idClient = AppView.scanner.nextLine().trim();
		return idClient;
	}
}
