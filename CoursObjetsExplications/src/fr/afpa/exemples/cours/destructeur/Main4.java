package fr.afpa.exemples.cours.destructeur;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.List;

public class Main4 {
	public static void main(String[] args) {
		ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();
		List<LargeMaClassFinalizer> references = new ArrayList<>();
		List<Object> largeObjects = new ArrayList<>();

		for (int i = 0; i < 10; ++i) {
			MaClasse largeObject = new MaClasse();
		    largeObjects.add(largeObject);
		    references.add(new LargeMaClassFinalizer(largeObject, referenceQueue));
		}

		largeObjects = null;
		System.gc();

		Reference<?> referenceFromQueue;
		for (PhantomReference<MaClasse> reference : references) {
		    System.out.println(reference.isEnqueued());
		}

		while ((referenceFromQueue = referenceQueue.poll()) != null) {
		    ((LargeMaClassFinalizer)referenceFromQueue).finalizeResources();
		    referenceFromQueue.clear();
		}
	}
}
