package fr.afpa.exercices_modelisation.traduction_class;

import java.util.Date;

public class Client {
	
	private int id;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	private String mail;
	//bi
	//private CompteBancaire[] compteBancaires;
	//bi
	//private Voiture[] voitures; 
	//bi
	private Partage[] partages;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public Partage[] getPartages() {
		return partages;
	}
	public void setPartages(Partage[] partages) {
		this.partages = partages;
	}

}
